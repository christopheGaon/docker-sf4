#!/bin/sh

cat ${PHP_INI_DIR}/php.ini > ${PHP_INI_DIR}/conf.d/99-overrides.ini

if [ -z "$XDEBUG_REMOTE_HOST" -a "$XDEBUG" -eq '1' ]; then
    export XDEBUG_REMOTE_HOST=`/sbin/ip route|awk '/default/ { print $3 }'`

    set +e
    # On Windows and MacOS, check that host.docker.internal exists. it true, use this.
    # Linux systems can report the value exists, but it is bound to localhost. In this case, ignore.
    # It will be avaible on Linux too in Docker >= 20
    # https://github.com/docker/for-linux/issues/264#issuecomment-598864064
    host -t A host.docker.internal &> /dev/null
    if [[ $? == 0 ]]; then
        # The host exists.
        DOCKER_HOST_INTERNAL=`host -t A host.docker.internal | awk '/has address/ { print $4 }'`
        if [ "$DOCKER_HOST_INTERNAL" != "127.0.0.1" ]; then
            export XDEBUG_REMOTE_HOST=$DOCKER_HOST_INTERNAL
        fi
    fi
    set -e

    php ${PHP_INI_DIR}/php-ini-overrides.php >> ${PHP_INI_DIR}/conf.d/99-overrides.ini
fi

# Let's find the user to use for commands.
# If $DOCKER_USER, let's use this. Otherwise, let's find it.
if [ -z "$DOCKER_USER" ]; then
    # On MacOSX, the owner of the current directory can be completely random (it can be root or docker depending on what happened previously)
    # But MacOSX does not enforce any rights (the docker user can edit any file owned by root).
    # On Windows, the owner of the current directory is root if mounted
    # But Windows does not enforce any rights either

    # Let's make a test to see if we have those funky rights.
    set +e
    mkdir -p testing_file_system_rights.foo
    chmod 700 testing_file_system_rights.foo
    su docker -c "touch testing_file_system_rights.foo/somefile > /dev/null 2>&1"
    HAS_CONSISTENT_RIGHTS=$?

    if [ "$HAS_CONSISTENT_RIGHTS" -ne "0" ]; then
        # If not specified, the DOCKER_USER is the owner of the current working directory
        DOCKER_USER=`ls -dl $(pwd) | cut -d " " -f 3`

        if [ -z "$DOCKER_USER" ]; then
            DOCKER_USER=`ls -dl $(pwd) | cut -d " " -f 5`
        fi
    else
        # we are on a Mac or Windows,
        # Most of the cases, we don't care about the rights (they are not respected)
        FILE_OWNER=`ls -dl testing_file_system_rights.foo/somefile | cut -d " " -f 3`
        if [[ "$FILE_OWNER" == "root" ]]; then
            # if the created user belongs to root, we are likely on a Windows host.
            # all files will belong to root, but it does not matter as everybody can write/delete those (0777 access rights)
            DOCKER_USER=docker
        else
            # In case of a NFS mount (common on MacOS), the created files will belong to the NFS user.
            DOCKER_USER=$FILE_OWNER
        fi
    fi

    rm -rf testing_file_system_rights.foo
    set -e

    unset HAS_CONSISTENT_RIGHTS
fi

uid=`id -ur ${DOCKER_USER}`
gid=`id -gr ${DOCKER_USER}`

if [ $uid == 0 ] && [ $gid == 0 ]; then
    if [ $# -eq 0 ]; then
        php-fpm --allow-to-run-as-root
    else
        echo "using $@"
        exec "$@"
    fi
fi

username=`id -un ${DOCKER_USER}`
group=`id -gn ${DOCKER_USER}`

sed -i -r "s/$username:x:\d+:\d+:/$username:x:$uid:$gid:/g" /etc/passwd
sed -i -r "s/$group:x:\d+:/$group:x:$gid:/g" /etc/group

sed -i "s/user = www-data/user = $username/g" /usr/local/etc/php-fpm.d/www.conf
sed -i "s/group = www-data/group = $group/g" /usr/local/etc/php-fpm.d/www.conf

# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#user
user=$(grep ":x:$uid:" /etc/passwd | cut -d: -f1)
if [ $# -eq 0 ]; then
    php-fpm
else
    echo gosu $user "$@"
    exec gosu $user "$@"
fi
