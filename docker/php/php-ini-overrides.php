<?php

if (in_array('xdebug', get_loaded_extensions())) {
    printf('%s# Xdebug%s', PHP_EOL, PHP_EOL);
    printf('xdebug.remote_host=%s%s',  getenv('XDEBUG_REMOTE_HOST'), PHP_EOL);
    printf('xdebug.remote_enable=on%s', PHP_EOL);
    printf('xdebug.remote_autostart=on%s', PHP_EOL);
}
